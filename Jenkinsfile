#!/usr/bin/env groovy

pipeline {
  agent any

  // env vars available to any pipeline stage
  environment {
    // grant Terraform access to AWS acct to create resources
    AWS_ACCESS_KEY_ID = credentials("aws_access_key_global")
    AWS_SECRET_ACCESS_KEY = credentials("	aws_secret_key_global")
  }

  stages {
    // ~ 12 mins
    stage("provision EKS cluster") {
      steps {
        script {
          echo "provisioning VPC, EKS cluster, and NodeGroup..."
          // initialize backend/ aws provider/ modules
          sh "terraform init"
          sh "terraform apply --auto-approve"
          sh "aws configure set default.region us-east-1"
          // update kubeconfig file in Jenkins container to use created cluster as current-context
          sh "aws eks update-kubeconfig --name java-app-eks --kubeconfig ~/.kube/config"
        }
      }
    }
    stage("deploy K8s components") {
      steps {
        script {
          echo "provisioning EKS addon and deploying K8s components..."
          dir("tf-apply-second") {
            // initialize backend/ helm provider
            sh "terraform init"
            // create addon (creates EBS vols to attach K8s vols to) & deploy 2 MySQL sts's
            // MySQL pods deploy on NodeGroup EC2s
            sh "terraform apply --auto-approve"
          }
          dir("k8s-components") {
            sh "kubectl apply -f secret.yaml"
            sh "kubectl apply -f configmap.yaml"
            // Java app replicas deploy on Fargate nodes (1 replica/ node)
            sh "kubectl apply -f java-gradle-app.yaml"
            sh "kubectl apply -f ingress.yaml"
          }
        }
      }
    }
    // ~12 mins
    // stage("destroy all resources") {
    //   steps {
    //     script {
    //       dir("tf-apply-second") {
    //         sh "terraform init"
    //         sh "terraform destroy --auto-approve"
    //       }
    //       sh "terraform init"
    //       sh "terraform destroy --auto-approve"
    //     }
    //   }
    // }
  }
}