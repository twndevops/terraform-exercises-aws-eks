#### Notes for "Module 12 - IaC w/ Terraform" exercises

- MODULE 11:

  - Provision AWS EKS cluster using `eksctl` config YAML.
  - Deploy ONLY Java-Gradle app to cluster in Jenkins pipeline.
  - Deploy all other K8s components manually using `kubectl`.

- MODULE 12:

  - Provision AWS EKS cluster using TF config files.
    - Use `helm` provider to deploy MySQL and Nginx-Ingress Controller.
  - Create new pipeline to provision/ adjust infra/ cluster AND deploy/ update K8s components.

This Terraform project provisioning/configuring the EKS cluster is separate from the [Java-Gradle app](https://gitlab.com/twndevops/kubernetes-exercises).

EXERCISE 1: Create TF project to spin up an EKS cluster. (Reference to set up volumes: https://gitlab.com/twn-devops-bootcamp/latest/12-terraform/terraform-exercises/-/tree/main.)

- Used AWS' `vpc` module to configure an EKS-optimized VPC.

  - Instead of using `terraform.tfvars` (which is gitignored), set var values as defaults in `variables.tf` for future reference.
  - Set `internal-elb` tag on private subnets, but no `elb` tag on public subnets since `java-gradle-app-svc` is ClusterIP/ Internal, not LoadBalancer. The cluster will route external requests via Ingress rules evaluated by an Ingress Controller pod.
    - FOLLOWUP: Is `elb` tag needed to create a public IP for the cluster that the Ingress rule can use as a `host` to route all incoming browser requests?

- Used AWS' `eks` module to configure the cluster.

  - Added `fargate_profiles` input to configure 1 Fargate profile for the stateless Java-Gradle app (i.e. Fargate will schedule pods matching `default` namespace and `profile: fargate` selectors on Fargate nodes).
  - It's not possible to mount EBS vols to Fargate pods.

- Used AWS' `eks` module > [`eks-managed-node-group` submodule](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest/submodules/eks-managed-node-group) to configure the EKS-managed NodeGroup.

  - The submodule offered input `iam_role_additional_policies` to add the permissions policy `AmazonEBSCSIDriverPolicy` to the NodeGroup so its node processes could create/ attach vols.
  - NOTE: NodeGroup EC2s will run 2 MySQL sts's (primary and secondary) and Nginx-Ingress Controller pod. (Not deploying `phpmyadmin` in this module's exercises.)

- Ran `terraform apply` to start up VPC and cluster.

- Ran `aws eks update-kubeconfig --name java-app-eks` to connect locally to cluster. Confirmed connection using `kubectl get nodes`.

- Used AWS provider's resource `aws_eks_addon` to add the EBS CSI (Container Storage Interface) driver to the EKS cluster as an addon.

  - The driver provisions K8s storage (i.e. attaches K8s pv's to EBS vols) and manages the EBS vols' lifecycle.
  - `eks` module's `cluster_addons` input is deprecated.
  - The cluster that `aws-eks-addon` references must exist before creating the addon!

- Deploy MySQL with 3 replicas with volumes for data persistence using Helm.

  - Used [`helm` provider](https://registry.terraform.io/providers/hashicorp/helm/latest).

    - `helm` provider block > `kubernetes` provider block has `config_path` pointing to local kubeconfig file.
      - If `config_context` is not specified, the provider will use the default/ current context.
    - Passed `mysqldb.yaml` file for values input to `helm_release` resource for MySQL.
    - `persistence.enabled` is `true` by default for primary and secondary sts's. Cluster addon `aws-ebs-csi-driver` creates the EBS vols.
      - Used `kubectl get pvc` to confirm MySQL `pvc`'s were provisioned (Status: Bound). StorageClass is `gp2`.
      - Checked AWS UI EC2 > left nav EBS Volumes. 4 vols created: 1 snapshot for the NodeGroup and 1 for each `mysql` replica.

  - `k8s-components/secret.yaml` and `k8s-components/configmap.yaml` enable `java-gradle-app` replicas to connect to `mysql` replicas.

- WIP: Deploy Ingress-Nginx controller (1 pod/1 Internal svc/ 1External svc) using Helm.

  - Used [`helm` provider](https://registry.terraform.io/providers/hashicorp/helm/latest).

    - ISSUE: Though controller pod w/ Nodeport External svc and Ingress rule in `k8s-components/ingress.yaml` are created, cannot access Java app via browser.
      - Troubleshooting references: https://stackoverflow.com/questions/70870569/helm-chart-will-install-manually-will-not-install-via-terraform, https://github.com/Azure/AKS/issues/2903

EXERCISE 2: Configure remote data store (S3 bucket) so all users/ Jenkins server can access latest TF state.

- If remote state is not configured, Jenkins will create its own local TF state in its locally checked out copy of the remote repo.

  - Once Jenkins starts updating remote state store, its local state files (if any) will no longer be updated.

- In AWS UI > S3, manually created new bucket.

  - BEST PRACTICES: blocked all public access and enabled versioning to back up state files.
  - Disabled bucket key (not used).

- Configured `backend` of type `s3` in BOTH root proj dir's `providers.tf` AND `tf-apply-second` dir's `helm-releases.tf` b/c TF is creating resources using config files in both dirs.

- NOTE: If any local `terraform apply` cmds/ pipeline builds were run using local state, destroy all existing infra before committing `backend` config to repo.

  - Optionally, can migrate local state to remote store on `terraform init` but will have to confirm migration manually (more complex to manage cleanly).

- Pipeline build initializes backend 2x. After running, check `java-app-eks-store` bucket > `state` dir > download `terraform.tfstate` and `apply-second.tfstate` JSONs to confirm resources were created in both stages.

- To access remote state locally:
  - `terraform init` in root proj dir/ `tf-apply-second` dir
  - `terraform state list` connects to bucket file and returns list of created resources

EXERCISE 3: Create a Jenkins pipeline in which Terraform provisions/ updates AWS infra/ EKS (K8s) cluster changes.

- This pipeline is separate from the Java-Gradle app pipeline created in Module 11 exercises.

- Regarding `provision EKS cluster` stage:

  - Installed AWS CLI in Jenkins container so Jenkins could update `~/.kube/config`'s `current-context` w/ created cluster info.

    - Logged into container as root user: `docker exec -it -u 0 ac0a377af2db /bin/bash`
    - `apt install awscli`. Checked installation succeeded w/ `aws --version`.
    - Configured `twnadmin` acct w/ `aws configure`.
    - Though region was configured in `~/.aws/config`, had to explicitly set default region in the stage for Jenkins to continue: `sh "aws configure set default.region us-east-1"`.

  - Encountered ISSUE: `Can't open kubeconfig for writing: [Errno 13] Permission denied: '/var/jenkins_home/.kube/config'`
    - Logged into container as jenkins user. File is owned by root user/group.
    - `chown jenkins:jenkins config` wasn't permitted. Deleted write-protected config file, created new empty config as jenkins user. `ls -l` indicated jenkins user had `rw-` permissions (needed write to update kubeconfig to cluster created from pipeline).

- Regarding `deploy K8s components` stage:

  - For `java-gradle-app deploy`, set `upnata/bootcamp-docker-java-mysql-project` to be a public repo to simplify pulling image.
  - Updated env var `DB_PWD` to get value from `mysql-3` secret.
  - `kubectl get pods -o wide` confirmed `java-gradle-app` pods deployed on Fargate nodes.
  - ISSUE: All images in `upnata/bootcamp-docker-java-mysql-project` repo cause `CrashLoopBackOff` error, pod containers run/fail/restart on loop, though all are built for `linux/amd64`.
    - WORKAROUND: Tried `upnata/module-8-java-mvn-app:1.0-SNAPSHOT` (built for `linux/amd64`), runs successfully in Fargate pod containers.

- Uncomment `destroy all resources` stage, comment out previous 2 stages and run pipeline to clean up.

  - In AWS UI, EC2 > delete EBS volumes for MySQL pvc's.

- FOLLOWUPS:

1. Adjust TF configs so infra can be replicated across 3 diff envs (for ex. dev, staging, prod).

- Parametrize cluster name (including in `vpc` module tags) to include env.

2. Check if it's possible to use EKS cluster's endpoint as the host for the Ingress rule.

- Solution `vpc` module does have tag to create ELB.
