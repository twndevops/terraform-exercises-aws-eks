data "aws_availability_zones" "zones" {}

module "java-app-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.5.0"

  name = "java-app-vpc"
  cidr = var.vpc_cidr

  # azs = data.aws_availability_zones.zones.names
  # us-east-1e did not have sufficient capacity to support the cluster
  azs = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1f"]
  public_subnets = var.pub_subnet_cidrs
  private_subnets = var.priv_subnet_cidrs

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  # tags label all created resources w/in cluster
  tags = {
    "kubernetes.io/cluster/java-app-eks" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/java-app-eks" = "shared"
    # java-gradle-app-svc is Internal, will use Ingress controller/ rules to route reqs to EKS cluster endpoint
    "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/java-app-eks" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}