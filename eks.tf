module "java-app-eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.21.0"

  cluster_name = "java-app-eks"
  cluster_version = "1.28"

  # private access to API server endpoint is enabled by default, public access is NOT (must enable for `kubectl` to connect to cluster)
  cluster_endpoint_public_access = true

  vpc_id = module.java-app-vpc.vpc_id
  # list priv subnets on which to start worker nodes/ schedule workloads
  subnet_ids = module.java-app-vpc.private_subnets

  # EKS-managed NodeGroup
  # eks_managed_node_groups = {
  #   java-app-nodes = {
  #     desired_size = 3
  #     min_size = 1
  #     max_size = 3
  #     instance_types = ["m5.large"]
  #   }
  # }

  # Fargate profile
  fargate_profiles = {
    java-app-profile = {
      name = "java-app-profile"
      selectors = [
        {
          namespace = "default"
          labels = {
            profile = "fargate"
          }
        }
      ]
    }
  }

  tags = {
    microservice = "java-gradle-app"
  }
}