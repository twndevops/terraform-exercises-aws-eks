# TF project metadata
terraform {
  # set version of TF CLI
  required_version = "1.6.6"

  # cannot use variables in backend block
  backend "s3" {
    # bucket name must be globally unique
    bucket = "java-app-eks-store"
    # path in bucket (dir structure)
    key = "state/terraform.tfstate"
    # required but need not be region where resources are
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.32.1"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.25.2"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.12.1"
    }
    cloudinit = {
      source = "hashicorp/cloudinit"
      version = "2.3.3"
    }
    tls = {
      source = "hashicorp/tls"
      version = "4.0.5"
    }
    time = {
      source = "hashicorp/time"
      version = "0.10.0"
    }
  }
}

provider "aws" {
  region = var.region
}