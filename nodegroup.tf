module "java-app-eks-managed-node-group" {
  source = "terraform-aws-modules/eks/aws//modules/eks-managed-node-group"
  version = "19.21.0"

  name = "java-app-nodes"
  cluster_name = "java-app-eks"

  # list priv subnets on which to start worker nodes/ schedule workloads
  subnet_ids = module.java-app-vpc.private_subnets

  # Following 2 inputs are reqd if you use this submodule outside of the parent EKS module context so that the security groups of the nodes join the cluster
  cluster_primary_security_group_id = module.java-app-eks.cluster_primary_security_group_id
  vpc_security_group_ids = [module.java-app-eks.node_security_group_id]

  desired_size = 3
  min_size = 1
  max_size = 3
  instance_types = ["m5.large"]

  # policy applied to NodeGroup to create EBS volumes for each node AND MySQL sts pod scheduled on each node
  iam_role_additional_policies = {
    AmazonEBSCSIDriverPolicy = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
  }

  tags = {
    microservice = "java-gradle-app"
  }
}