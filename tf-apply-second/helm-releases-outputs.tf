# Reference: https://registry.terraform.io/providers/hashicorp/helm/latest/docs/data-sources/template

output "mysql-release-template" {
  value = data.helm_template.mysql-chart-template.manifests
}