# Reference: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_addon

resource "aws_eks_addon" "ebs-driver" {
  cluster_name = "java-app-eks"
  addon_name = "aws-ebs-csi-driver"
}