# This provider is in this file b/c in Jenkins pipeline, must set kubeconfig current-context to created EKS cluster, then initialize helm provider to use the context

terraform {
  # set version of TF CLI
  required_version = "1.6.6"

  # cannot use variables in backend block
  backend "s3" {
    # bucket name must be globally unique
    bucket = "java-app-eks-store"
    # path in bucket (dir structure)
    key = "state/apply-second.tfstate"
    # required but need not be region where resources are
    region = "us-east-1"
  }
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

data "helm_template" "mysql-chart-template" {
  name = "mysql-3"
  repository = "https://charts.bitnami.com/bitnami"
  chart = "mysql"
}

resource "helm_release" "mysql-3" {
  // 3 replicas of MySQL deployed
  name = "mysql-3"
  repository = "https://charts.bitnami.com/bitnami"
  chart = "mysql"
  version = "9.16.1"
  # namespace = "default"

  # file func merges YAML's custom vals with chart's default vals
  values = [
    "${file("mysqldb.yaml")}"
  ]
}

resource "helm_release" "ingress-nginx" {
  name = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart = "ingress-nginx"
  version = "4.9.0"
  # namespace = "default"

  values = [
    "${file("ingress-controller.yaml")}"
  ]
}